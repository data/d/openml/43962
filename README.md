# OpenML dataset: energy_efficiency

https://www.openml.org/d/43962

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Source**

This dataset was obtained from the UCI Repository.

The dataset was created by Angeliki Xifara (angxifara '@' gmail.com, Civil/Structural Engineer) and was processed by Athanasios Tsanas (tsanasthanasis '@' gmail.com, Oxford Centre for Industrial and Applied Mathematics, University of Oxford, UK).
Data Set Information:
We perform energy analysis using 12 different building shapes simulated in Ecotect. The buildings differ with respect to the glazing area, the glazing area distribution, and the orientation, amongst other parameters. We simulate various settings as functions of the afore-mentioned characteristics to obtain 768 building shapes. The dataset comprises 768 samples and 8 features, aiming to predict two real valued responses. It can also be used as a multi-class classification problem if the response is rounded to the nearest integer.

**Attribute Information**

The dataset contains eight attributes and two responses. In the original data, the aim is to use the eight features to predict each of the two responses.
For this version of the dataset however, only the *Heating Load* is used as the target, while the *Cooling Load* is being ignored. In addition, the variables were renamed to be more meaningful.

**Relevant Papers**

* A. Tsanas, A. Xifara: 'Accurate quantitative estimation of energy performance of residential buildings using statistical machine learning tools', Energy and Buildings, Vol. 49, pp. 560-567, 2012
Citation Request:

* A. Tsanas, A. Xifara: 'Accurate quantitative estimation of energy performance of residential buildings using statistical machine learning tools', Energy and Buildings, Vol. 49, pp. 560-567, 2012

For further details on the data analysis methodology:

* A. Tsanas, 'Accurate telemonitoring of Parkinsons disease symptom severity using nonlinear speech signal processing and statistical machine learning', D.Phil. thesis, University of Oxford, 2012

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43962) of an [OpenML dataset](https://www.openml.org/d/43962). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43962/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43962/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43962/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

